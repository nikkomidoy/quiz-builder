import {
    Component, OnInit, Input, Output, EventEmitter, ViewContainerRef, ViewChild, ElementRef
} from '@angular/core';
import { Router } from "@angular/router";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { SectionsService } from '../_services/sections.service';
import { QuestionsService } from '../_services/questions.service';
import { AnswersService } from '../_services/answers.service';

@Component({
    selector: 'app-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

    @Input() private sectionsId: any;
    @Output() displaySection:EventEmitter <boolean> = new EventEmitter();
    @ViewChild('closeBtn') closeBtn: ElementRef;
    @ViewChild('closeDeleteModal') closeDeleteModal: ElementRef;
    private questions:any = [];
    private question_by_id:any = {}
    private current_select_q_id:number;
    updateQuestionModel: FormGroup;
    addQuestionModel: FormGroup;
    deleteQuestionModel: FormGroup;

    constructor(
        private _sectionService: SectionsService,
        private _questionService: QuestionsService,
        private _answerService: AnswersService,
        private _toastr: ToastsManager,
        private vcr: ViewContainerRef,
        private _router: Router) {
        this.updateQuestionModel = new FormGroup({
            q_name: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            weightage: new FormControl('', [
                Validators.required
            ]),
            section: new FormControl(''),
            order: new FormControl(''),
            description: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            content: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ])
        });
        this.addQuestionModel = new FormGroup({
            q_name: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            weightage: new FormControl('', [
                Validators.required
            ]),
            section: new FormControl(''),
            order: new FormControl(''),
            description: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            content: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ])
        });
        this.deleteQuestionModel = new FormGroup({
            id: new FormControl('')
        });
        this._toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.getQuestionsInSections(this.sectionsId);
        document.getElementById('openAddModal').click();
    }
    onSubmit({ value, valid }: { value:any, valid: boolean }) {
        console.log(value, valid);
    }
    getQuestionsInSections(section_id:number) {
        this._sectionService.getQuestionsInSections(section_id).subscribe(
            data => {
                this.questions = data.json();
            },
            err => {
                console.log(err);
            }
        );
    }

    backToSection () {
        this.displaySection.emit(true);
    }

    updateQuestion() {
        this._questionService.updateQuestionById(
            this.updateQuestionModel.value, this.question_by_id.id).subscribe(
            data => {
                this._toastr.success('Success!', 'Question Updated!');
                this.getQuestionsInSections(this.sectionsId);
            },
            err => {
                console.log(err);
            }
        );
    }

    addQuestion() {
        this.addQuestionModel.controls['section'].patchValue(this.sectionsId);
        this.addQuestionModel.controls['order'].patchValue(10);
        this._questionService.addQuestion(this.addQuestionModel.value).subscribe(
            data => {
                this._toastr.success('Success!', 'Question Added!');
                this.getQuestionsInSections(this.sectionsId);
                this.closeBtn.nativeElement.click();
            },
            err => {
                console.log(err);
            }
        );
    }

    setQuestion(question_id:number) {
        this._questionService.getQuestionById(question_id).subscribe(
            data => {
                this.current_select_q_id = question_id;
                this.question_by_id = data.json();
                this.updateQuestionModel.controls['q_name'].patchValue(this.question_by_id.q_name);
                this.updateQuestionModel.controls['weightage'].patchValue(this.question_by_id.weightage);
                this.updateQuestionModel.controls['section'].patchValue(this.question_by_id.section);
                this.updateQuestionModel.controls['order'].patchValue(this.question_by_id.order);
                this.updateQuestionModel.controls['description'].patchValue(this.question_by_id.description);
                this.updateQuestionModel.controls['content'].patchValue(this.question_by_id.content);
            },
            err => {
                console.log(err);
            }
        );
    }

    setDeleteQuestion(question_id:number) {
        this.deleteQuestionModel.controls['id'].patchValue(question_id);
    }

    deleteQuestion() {
        this._questionService.deleteQuestionById(this.deleteQuestionModel.value.id).subscribe(
            data => {
                this._toastr.success('Success!', 'Question Deleted!');
                this.getQuestionsInSections(this.sectionsId);
                this.closeDeleteModal.nativeElement.click();
            },
            err => {
                console.log(err);
            }
        );
    }
}
