export class Question {
    id: number;
    content: string;
    q_name: string;
    description: string;
}
