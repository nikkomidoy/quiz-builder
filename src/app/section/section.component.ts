import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SectionsService } from '../_services/sections.service';
import { Router } from "@angular/router";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { TreeNode, ITreeState, ITreeOptions } from 'angular-tree-component';
import uuid from 'uuid';
import { FormGroup, FormControl, Validators } from "@angular/forms";


@Component({
    selector: 'app-section',
    templateUrl: './section.component.html',
    styleUrls: ['./section.component.css']
})

export class SectionComponent implements OnInit {
    private sections: any = [];
    private questions: any = [];
    private display_questions: any = [];
    private is_display_question = false;
    private current_section_id: number;
    private template_id:number = 1;
    private nodes: any = []
    addSectionModel: FormGroup;
    private isFirstSelect:boolean = false;
    private state: ITreeState = {
        hiddenNodeIds: {},
        activeNodeIds: {}
    };

    private options: ITreeOptions = {
        allowDrag: (node) => node.isLeaf,
        allowDrop: (element, { parent, index }) => {
            return parent.hasChildren;
        },
        animateSpeed: 70,
        dropSlotHeight: 3
    };

    @ViewChild('closeBtn') closeBtn: ElementRef;
    @ViewChild('sectionnodes') sectionnodes: ElementRef;

    constructor(
        private _sectionService: SectionsService,
        private _toastr: ToastsManager,
        private vcr: ViewContainerRef,
        private _router: Router) {

        this.addSectionModel = new FormGroup({
            name: new FormControl('',[
                Validators.required,
                Validators.minLength(2)
            ]),
            description: new FormControl('',[
                Validators.required,
                Validators.minLength(2)
            ]),
            template: new FormControl(''),
            order: new FormControl('')
        });
        this._toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.getSections(this.template_id);
    }

    ngAfterViewChecked() {
        if(this.isFirstSelect == false) {
            //insert click on first node
            this.isFirstSelect = true;
        }
    }

    moveNode(event:any) {
        let moving_node = event.node;
        let node_index = event.to.index;
        this.sections.forEach((item, index) => {
            if (index >= node_index) {
                item.order = ++index;
                this._sectionService.updateSectionById(item, item.id).subscribe(
                    data => {
                        this.getSections(this.template_id);
                    },
                    err => {
                        console.log(err);
                    }
                );
            }
        });
        this._toastr.success('Success!', 'Section order updated!');
    }

    getSections(template_id:number) {
        this._sectionService.getSections(template_id).subscribe(
            data => {
                this.sections = data.json();
                this.sections.filter(x => x.order === 1).map(
                    x => {
                        this.getQuestionsInSections(x.id);
                    }
                )
                this.sections.sort(function(first, second) {
                    return first.order - second.order;
                });
            },
            err => {
                console.log(err);
            }
        );
    }

    getQuestionsInSections(section_id:number) {
        this._sectionService.getQuestionsInSections(section_id).subscribe(
            data => {
                this.questions = data.json();
                this.current_section_id = section_id;
            },
            err => {
                console.log(err);
            }
        );
    }

    addSection() {
        let lastOrder = this.sections[this.sections.length-1].order;
        this.addSectionModel.controls['template'].patchValue(this.template_id);
        this.addSectionModel.controls['order'].patchValue(lastOrder+1);
        this._sectionService.addSection(this.addSectionModel.value).subscribe(
            data => {
                this.getSections(this.template_id);
                this.closeBtn.nativeElement.click();
            },
            err => {
                console.log(err);
            }
        );
    }

    addQuestion() {
        this.is_display_question = true;
    }

    sectionDisplay(event:boolean) {
        if (event) {
            this.getQuestionsInSections(this.current_section_id);
            this.is_display_question = false;
        }
    }
}
