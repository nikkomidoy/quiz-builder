import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Rx";


@Injectable()
export class SectionsService {

    private API_URL: string = 'http://uat.dataprotectionmgmt.com';

    constructor(private http:Http) { }

    getSections(id:number) {
        return this.http.get(
            this.API_URL + '/ca-templates/api/temp/'+ id +'/sects/',
            this.headers()
        );
    }

    getQuestionsInSections(id:number) {
        return this.http.get(
            this.API_URL + '/ca-templates/api/sect/' + id + '/quests/',
            this.headers()
        );
    }

    addSection(q_model:any) {
        return this.http.post(
            this.API_URL + '/ca-templates/api/sect/',
            JSON.stringify(q_model),
            this.headers()
        );
    }

    updateSectionById(s_model:any, id:number) {
        return this.http.put(
            this.API_URL + '/ca-templates/api/sect/' + id + '/',
            JSON.stringify(s_model),
            this.headers()
        );
    }

    private headers() {
        let headers = new Headers({
            'Authorization': 'Basic ' + btoa('dpmsadmin' + ":" + 'DPMS123!'),
            'Content-Type': 'application/json'
        });
        return new RequestOptions({ 'headers': headers });
    }
}
