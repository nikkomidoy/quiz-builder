import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Rx";


@Injectable()
export class AnswersService {

    private API_URL: string = 'http://uat.dataprotectionmgmt.com';

    constructor(private http:Http) { }

    getAnswersInQuestions(id:number) {
        return this.http.get(
            this.API_URL + '/ca-templates/api/quest/'+ id +'/ans/',
            this.headers()
        );
    }

    getAnswerById(id:number) {
        return this.http.get(
            this.API_URL + '/ca-templates/api/ans/'+ id +'/',
            this.headers()
        );
    }

    updateAnswerById(a_model:any, id:number) {
        return this.http.put(
            this.API_URL + '/ca-templates/api/ans/'+ id +'/',
            JSON.stringify(a_model),
            this.headers()
        );
    }

    addAnswer(q_model:any) {
        return this.http.post(
            this.API_URL + '/ca-templates/api/ans/',
            JSON.stringify(q_model),
            this.headers()
        );
    }

    deleteAnswerById(id:number) {
        return this.http.delete(
            this.API_URL + '/ca-templates/api/ans/'+ id +'/',
            this.headers()
        );
    }

    private headers() {
        let headers = new Headers({
            'Authorization': 'Basic ' + btoa('dpmsadmin' + ":" + 'DPMS123!'),
            'Content-Type': 'application/json'
        });
        return new RequestOptions({ 'headers': headers });
    }
}
