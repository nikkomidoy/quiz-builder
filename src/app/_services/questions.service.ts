import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Rx";


@Injectable()
export class QuestionsService {

    private API_URL: string = 'http://uat.dataprotectionmgmt.com';

    constructor(private http:Http) { }

    getQuestionById(id:number) {
        return this.http.get(
            this.API_URL + '/ca-templates/api/quest/'+ id +'/',
            this.headers()
        );
    }

    updateQuestionById(q_model:any, id:number) {
        return this.http.put(
            this.API_URL + '/ca-templates/api/quest/'+ id +'/',
            JSON.stringify(q_model),
            this.headers()
        );
    }

    addQuestion(q_model:any) {
        return this.http.post(
            this.API_URL + '/ca-templates/api/quest/',
            JSON.stringify(q_model),
            this.headers()
        );
    }

    deleteQuestionById(id:number) {
        return this.http.delete(
            this.API_URL + '/ca-templates/api/quest/'+ id +'/',
            this.headers()
        );
    }

    private headers() {
        let headers = new Headers({
            'Authorization': 'Basic ' + btoa('dpmsadmin' + ":" + 'DPMS123!'),
            'Content-Type': 'application/json'
        });
        return new RequestOptions({ 'headers': headers });
    }
}
