import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from "@angular/http";

import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import * as FroalaEditor from 'froala-editor/js/froala_editor.pkgd.min';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TreeModule } from 'angular-tree-component';
import { CKEditorModule } from 'ng2-ckeditor';
import { InlineEditorModule } from 'ng2-inline-editor';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';

import {routing} from "./app.routing";
import {RouterModule} from "@angular/router";
import { SectionComponent } from './section/section.component';
import { QuestionComponent } from './question/question.component';
import { SectionsService } from './_services/sections.service';
import { QuestionsService } from './_services/questions.service';
import { AnswersService } from './_services/answers.service';
import { AnswerComponent } from './answer/answer.component';


@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        SidebarComponent,
        HomeComponent,
        SectionComponent,
        QuestionComponent,
        AnswerComponent
    ],
    imports: [
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        ToastModule.forRoot(),
        CKEditorModule,
        InlineEditorModule,
        BrowserAnimationsModule,
        BrowserModule,
        RouterModule,
        routing,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        TreeModule
    ],
    providers:[
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        SectionsService,
        AnswersService,
        QuestionsService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
