import { Component, OnInit, Input, ViewContainerRef, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AnswersService } from '../_services/answers.service';

@Component({
    selector: 'app-answer',
    templateUrl: './answer.component.html',
    styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

    private answers_by_q_id:any = [];
    private answer_by_id:any = {}
    private current_question_id:number;
    @ViewChild('closeAddBtn') closeAddBtn: ElementRef;
    @ViewChild('closeDeleteBtn') closeDeleteBtn: ElementRef;
    updateAnswerModel: FormGroup;
    update_answer_label:any = '';
    addAnswerModel: FormGroup;
    deleteAnswerModel: FormGroup;

    constructor(
        private _answerService: AnswersService,
        private _toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.updateAnswerModel = new FormGroup({
            label: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            value: new FormControl('', [
                Validators.required
            ]),
            question: new FormControl(''),
            order: new FormControl('')
        });
        this.addAnswerModel = new FormGroup({
            label: new FormControl('', [
                Validators.required,
                Validators.minLength(2)
            ]),
            value: new FormControl('', [
                Validators.required
            ]),
            question: new FormControl(''),
            order: new FormControl('')
        });
        this.deleteAnswerModel = new FormGroup({
            id: new FormControl('')
        });
    }

    ngOnInit() {
    }

    @Input()
    set questionId(value:number) {
        if (value != undefined) {
            this.current_question_id = value;
            this.setAnswers(this.current_question_id);
        }
    }

    saveEditable(value, key) {
        this.updateAnswerModel.controls[key].patchValue(value);
        this.updateAnswer();
    }

    setAnswers(question_id:number) {
        this._answerService.getAnswersInQuestions(question_id).subscribe(
            data => {
                this.answers_by_q_id = data.json();
            },
            err => {
                console.log(err);
            }
        );
    }

    setUpdateAnswer(answer_id:number) {
        this._answerService.getAnswerById(answer_id).subscribe(
            data => {
                this.answer_by_id = data.json();
                this.updateAnswerModel.controls['label'].patchValue(this.answer_by_id.label);
                this.updateAnswerModel.controls['value'].patchValue(this.answer_by_id.value);
                this.updateAnswerModel.controls['question'].patchValue(this.answer_by_id.question);
                this.updateAnswerModel.controls['order'].patchValue(this.answer_by_id.order);
            },
            err => {
                console.log(err);
            }
        );
    }

    updateAnswer() {
        this._answerService.updateAnswerById(
            this.updateAnswerModel.value, this.answer_by_id.id).subscribe(
            data => {
                this._toastr.success('Success!', 'Answer Updated!');
                this.setAnswers(this.current_question_id);
            },
            err => {
                console.log(err);
            }
        );
    }

    addAnswer() {
        this.addAnswerModel.controls['question'].patchValue(this.current_question_id);
        this.addAnswerModel.controls['order'].patchValue(1);
        this._answerService.addAnswer(this.addAnswerModel.value).subscribe(
            data => {
                this._toastr.success('Success!', 'Answer Added!');
                this.setAnswers(this.current_question_id);
                this.closeAddBtn.nativeElement.click();
            },
            err => {
                console.log(err);
            }
        );
    }

    setDeleteAnswer(answer_id:number) {
        this.deleteAnswerModel.controls['id'].patchValue(answer_id);
    }

    deleteAnswer() {
        this._answerService.deleteAnswerById(this.deleteAnswerModel.value.id).subscribe(
            data => {
                this._toastr.success('Success!', 'Answer Deleted!');
                this.setAnswers(this.current_question_id);
                this.closeDeleteBtn.nativeElement.click();
            },
            err => {
                console.log(err);
            }
        );
    }
}
