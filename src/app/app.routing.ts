import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SectionComponent } from './section/section.component';
import { QuestionComponent } from './question/question.component';


export const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'sections', component: SectionComponent},
  {path: 'questions', component: QuestionComponent}
];

export const routing = RouterModule.forRoot(routes);
