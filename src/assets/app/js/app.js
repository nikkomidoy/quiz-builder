var app = angular.module('sections', ['ngSanitize','ui.bootstrap']);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRFToken'] = '{{ csrf_token|escapejs }}';
}]);

app.controller("SectionController",['$scope', function($scope){

}]);
